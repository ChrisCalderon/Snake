from __future__ import annotations
import tkinter
from tkinter import ttk
from typing import Deque, Iterator, Tuple
from random import randrange
from collections import deque
from math import gcd
from enum import Enum
from dataclasses import dataclass
from threading import Lock

TICK = 100  # in milliseconds
MIN_SIDE = 25


class CoolLock:
    def __init__(self):
        self.__lock = Lock()

    def __call__(self, func):
        def new_func(*args, **kwds):
            with self.__lock:
                return func(*args, **kwds)
        return new_func


class Colors(Enum):
    BACKGROUND = 'blue'
    SNAKE = 'green'
    FOOD = 'red'


@dataclass(frozen=True)
class Coord:
    x: int = 0
    y: int = 0

    def __add__(self: Coord, other: Coord) -> Coord:
        return Coord(self.x + other.x, self.y + other.y)


UP = Coord(0, -1)
DOWN = Coord(0, 1)
LEFT = Coord(-1, 0)
RIGHT = Coord(1, 0)


def choose_grid_size(root: tkinter.Tk) -> Tuple[int,int,int]:
    width = root.winfo_screenwidth()//2
    height = root.winfo_screenheight()//2
    temp = gcd(width, height)
    
    for i in range(1, temp):
        if temp%i:
            continue
        h = temp // i
        if min(width//h, height//h) > MIN_SIDE:
            return width, height, h

    return 625, 625, 25 # fallback game size


class SnakeApp:
    direction_lock = CoolLock()

    def __init__(self):
        self.root = tkinter.Tk()
        self.root.title('Snake')
        width, height, h = choose_grid_size(self.root)
        self.canvas = tkinter.Canvas(self.root, width=width, height=height)
        self.canvas.config(borderwidth=1)
        self.canvas.config(background=Colors.BACKGROUND.value)
        self.canvas.pack()
        self.tiles = [[self.canvas.create_rectangle(x, y, x+h, y+h, outline=Colors.BACKGROUND.value)
                           for x in range(0, width, h)]
                           for y in range(0, height, h)]
        self.grid_width = len(self.tiles[0])
        self.grid_height = len(self.tiles)
        m = self.grid_width//2
        self.snake: Deque[Coord] = deque([
            Coord(m, 2),
            Coord(m, 1),
            Coord(m, 0),
        ])
        for snake_part in self.snake:
            self.set_tile(snake_part, Colors.SNAKE)
        self.old_direction = self.direction = DOWN
        self.draw_food()
        self.root.bind('<KeyPress>', self.change_direction)
        self.root.after(TICK, self.update)

    def set_tile(self, x: Coord, c: Colors):
        tile_id = self.tiles[x.y][x.x]
        self.canvas.itemconfig(tile_id, fill=c.value)

    def get_tile(self, x: Coord) -> Colors:
        tile_id = self.tiles[x.y][x.x]
        c = self.canvas.itemcget(tile_id, 'fill')
        for color in Colors:
            if color.value == c:
                return color

    @direction_lock
    def change_direction(self, e):
        if self.direction in (UP, DOWN):
            if e.keysym == 'Left' and self.old_direction != RIGHT:
                self.direction = LEFT
            elif e.keysym == 'Right' and self.old_direction != LEFT:
                self.direction = RIGHT
        else:
            if e.keysym == 'Up' and self.old_direction != DOWN:
                self.direction = UP
            elif e.keysym == 'Down' and self.old_direction != UP:
                self.direction = DOWN
    
    @direction_lock
    def update(self):
        d = self.direction
        self.old_direction = d
        new_head = self.snake[0] + d
        if new_head.x < 0 or new_head.x >= self.grid_width:
            dead = True
        elif new_head.y < 0 or new_head.y >= self.grid_height:
            dead = True
        elif new_head in self.snake:
            dead = True
        else:
            dead = False

        if dead:
            self.set_tile(self.snake.pop(), Colors.BACKGROUND)
            return self.died()

        self.snake.appendleft(new_head)
        if self.get_tile(new_head) != Colors.FOOD:
            self.set_tile(self.snake.pop(), Colors.BACKGROUND)
        else:
            self.draw_food()
        self.set_tile(new_head, Colors.SNAKE)
        self.root.after(TICK, self.update)

    def died(self):
        win = tkinter.Toplevel()
        win.title('DEAD')
        message = ttk.Label(win, text='You died!', justify=tkinter.CENTER, anchor=tkinter.CENTER)
        message.grid(row=0, column=0, columnspan=2, stick=tkinter.E+tkinter.W)
        q = ttk.Button(win, text='Quit', command=lambda: self.root.destroy())
        q.grid(row=1, column=0)
        r = ttk.Button(win, text='Restart', command=lambda: win.destroy() or self.restart())
        r.grid(row=1, column=1)

    @direction_lock
    def restart(self):
        for y in range(len(self.tiles)):
            for x in range(len(self.tiles[0])):
                self.set_tile(Coord(x, y), Colors.BACKGROUND)
                    
        m = self.grid_width//2
        self.snake = deque([
            Coord(m, 2),
            Coord(m, 1),
            Coord(m, 0),
        ])
        self.direction = DOWN
        for s in self.snake:
            self.set_tile(s, Colors.SNAKE)
        self.draw_food()
        self.root.after(TICK, self.update)  

    def draw_food(self):
        h, w = self.grid_height, self.grid_width
        snake = self.snake
        while True:
           c = Coord(randrange(w), randrange(h))
           if c not in snake:
               self.set_tile(c, Colors.FOOD)
               break

    def run(self):
        try:
            self.root.mainloop()
        except:
            return


if __name__ == '__main__':
    s = SnakeApp()
    s.run()
